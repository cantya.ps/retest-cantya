# Kakak Asuh Application - Advanced Programming - 2019 - Fasilkom UI
## This repository is a modified version from the original https://github.com/spring-projects/spring-petclinic

This application about Academic Consulatation (Kakak Asuh Program)
This application is used to help students studying with mentors provided from
the KAKAK ASUH PROGRAM.
You can add new or search mentors and add new students for studying.
Time allocation of assistant is also available.

The following is the URL for testing my software product:
https://kakak-asuh-fasilkom.herokuapp.com/
