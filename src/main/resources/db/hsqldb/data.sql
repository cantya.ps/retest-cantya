INSERT INTO assistants VALUES (1, 'James', 'Carter', '8am to 12pm');
INSERT INTO assistants VALUES (2, 'Helen', 'Leary', '10am to 12pm');
INSERT INTO assistants VALUES (3, 'Linda', 'Douglas', '1pm to 5pm');
INSERT INTO assistants VALUES (4, 'Rafael', 'Ortega', '8am to 3pm');
INSERT INTO assistants VALUES (5, 'Henry', 'Stevens', '9am to 2pm');
INSERT INTO assistants VALUES (6, 'Sharon', 'Jenkins', '1pm to 4pm');

INSERT INTO specialties VALUES (1, 'Advance Programing');
INSERT INTO specialties VALUES (2, 'Data Structure Algorithm');
INSERT INTO specialties VALUES (3, 'Matdas');

INSERT INTO assistant_specialties VALUES (1, 1);
INSERT INTO assistant_specialties VALUES (2, 2);
INSERT INTO assistant_specialties VALUES (3, 3);
INSERT INTO assistant_specialties VALUES (4, 2);
INSERT INTO assistant_specialties VALUES (5, 1);
INSERT INTO assistant_specialties VALUES (6, 3);

INSERT INTO types VALUES (1, 'Semester 1');
INSERT INTO types VALUES (2, 'Semester 2');
INSERT INTO types VALUES (3, 'Semester 3');
INSERT INTO types VALUES (4, 'Semester 4');
INSERT INTO types VALUES (5, 'Semester 5');
INSERT INTO types VALUES (6, 'Semester 6');

INSERT INTO mentors VALUES (1, 'George', 'Franklin', '110 W. Liberty St.', 'Madison', '6085551023');
INSERT INTO mentors VALUES (2, 'Betty', 'Davis', '638 Cardinal Ave.', 'Sun Prairie', '6085551749');
INSERT INTO mentors VALUES (3, 'Eduardo', 'Rodriquez', '2693 Commerce St.', 'McFarland', '6085558763');
INSERT INTO mentors VALUES (4, 'Harold', 'Davis', '563 Friendly St.', 'Windsor', '6085553198');
INSERT INTO mentors VALUES (5, 'Peter', 'McTavish', '2387 S. Fair Way', 'Madison', '6085552765');
INSERT INTO mentors VALUES (6, 'Jean', 'Coleman', '105 N. Lake St.', 'Monona', '6085552654');
INSERT INTO mentors VALUES (7, 'Jeff', 'Black', '1450 Oak Blvd.', 'Monona', '6085555387');
INSERT INTO mentors VALUES (8, 'Maria', 'Escobito', '345 Maple St.', 'Madison', '6085557683');
INSERT INTO mentors VALUES (9, 'David', 'Schroeder', '2749 Blackhawk Trail', 'Madison', '6085559435');
INSERT INTO mentors VALUES (10, 'Carlos', 'Estaban', '2335 Independence La.', 'Waunakee', '6085555487');

INSERT INTO mentees VALUES (1, 'Leo', '2000-09-07', 1, 1);
INSERT INTO mentees VALUES (2, 'Basil', '2002-08-06', 6, 2);
INSERT INTO mentees VALUES (3, 'Rosy', '2001-04-17', 2, 3);
INSERT INTO mentees VALUES (4, 'Jewel', '2000-03-07', 2, 3);
INSERT INTO mentees VALUES (5, 'Iggy', '2000-11-30', 3, 4);
INSERT INTO mentees VALUES (6, 'George', '2000-01-20', 4, 5);
INSERT INTO mentees VALUES (7, 'Samantha', '1995-09-04', 1, 6);
INSERT INTO mentees VALUES (8, 'Max', '1995-09-04', 1, 6);
INSERT INTO mentees VALUES (9, 'Lucky', '1999-08-06', 5, 7);
INSERT INTO mentees VALUES (10, 'Mulligan', '1997-02-24', 2, 8);
INSERT INTO mentees VALUES (11, 'Freddy', '2000-03-09', 5, 9);
INSERT INTO mentees VALUES (12, 'Lucky', '2000-06-24', 2, 10);
INSERT INTO mentees VALUES (13, 'Sly', '2002-06-08', 1, 10);

INSERT INTO consultations VALUES (1, 7, '2013-01-01', 'homework study');
INSERT INTO consultations VALUES (2, 8, '2013-01-02', 'exam study');
INSERT INTO consultations VALUES (3, 8, '2013-01-03', 'quiz study');
INSERT INTO consultations VALUES (4, 7, '2013-01-04', 'project study');
