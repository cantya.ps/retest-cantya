DROP TABLE assistant_specialties IF EXISTS;
DROP TABLE assistants IF EXISTS;
DROP TABLE specialties IF EXISTS;
DROP TABLE consultations IF EXISTS;
DROP TABLE mentees IF EXISTS;
DROP TABLE types IF EXISTS;
DROP TABLE mentors IF EXISTS;


CREATE TABLE assistants (
  id         INTEGER IDENTITY PRIMARY KEY,
  first_name VARCHAR(30),
  last_name  VARCHAR(30),
  price      VARCHAR(30)
);
CREATE INDEX assistants_last_name ON assistants (last_name);

CREATE TABLE specialties (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(80)
);
CREATE INDEX specialties_name ON specialties (name);

CREATE TABLE assistant_specialties (
  assistant_id       INTEGER NOT NULL,
  specialty_id INTEGER NOT NULL
);
ALTER TABLE assistant_specialties ADD CONSTRAINT fk_assistant_specialties_assistants FOREIGN KEY (assistant_id) REFERENCES assistants (id);
ALTER TABLE assistant_specialties ADD CONSTRAINT fk_assistant_specialties_specialties FOREIGN KEY (specialty_id) REFERENCES specialties (id);

CREATE TABLE types (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(80)
);
CREATE INDEX types_name ON types (name);

CREATE TABLE mentors (
  id         INTEGER IDENTITY PRIMARY KEY,
  first_name VARCHAR(30),
  last_name  VARCHAR_IGNORECASE(30),
  address    VARCHAR(255),
  city       VARCHAR(80),
  telephone  VARCHAR(20)
);
CREATE INDEX mentors_last_name ON mentors (last_name);

CREATE TABLE mentees (
  id         INTEGER IDENTITY PRIMARY KEY,
  name       VARCHAR(30),
  birth_date DATE,
  type_id    INTEGER NOT NULL,
  mentor_id   INTEGER NOT NULL
);
ALTER TABLE mentees ADD CONSTRAINT fk_mentees_mentors FOREIGN KEY (mentor_id) REFERENCES mentors (id);
ALTER TABLE mentees ADD CONSTRAINT fk_mentees_types FOREIGN KEY (type_id) REFERENCES types (id);

CREATE INDEX mentees_name ON mentees (name);

CREATE TABLE consultations (
  id          INTEGER IDENTITY PRIMARY KEY,
  mentee_id      INTEGER NOT NULL,
  consultation_date  DATE,
  description VARCHAR(255)
);
ALTER TABLE consultations ADD CONSTRAINT fk_consultations_mentees FOREIGN KEY (mentee_id) REFERENCES mentees (id);
CREATE INDEX consultations_mentee_id ON consultations (mentee_id);
