/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.mentor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 */
@Controller
@RequestMapping("/mentors/{mentorId}")
class MenteeController {

    private static final String VIEWS_MENTEES_CREATE_OR_UPDATE_FORM = "mentees/createOrUpdateMenteeForm";
    private final MenteeRepository mentees;
    private final MentorRepository mentors;

    public MenteeController(MenteeRepository mentees, MentorRepository mentors) {
        this.mentees = mentees;
        this.mentors = mentors;
    }

    @ModelAttribute("menteez")
    public Collection<Mentee> populateMentee() {
        return this.mentees.findAll();
    }

    @ModelAttribute("types")
    public Collection<MenteeType> populateMenteeTypes() {
        return this.mentees.findMenteeTypes();
    }

    @ModelAttribute("mentor")
    public Mentor findMentor(@PathVariable("mentorId") int mentorId) { return this.mentors.findById(mentorId);
    }

    @InitBinder("mentor")
    public void initMentorBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @InitBinder("mentee")
    public void initMenteeBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(new MenteeValidator());
    }

    @GetMapping("/mentees/new")
    public String initCreationForm(Mentor mentor, ModelMap model) {
        Mentee mentee = new Mentee();
        mentor.addMentee(mentee);
        model.put("mentee", mentee);
        return VIEWS_MENTEES_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/mentees/new")
    public String processCreationForm(Mentor mentor, @Valid Mentee mentee, BindingResult result, ModelMap model) {
        if (StringUtils.hasLength(mentee.getName()) && mentee.isNew() && mentor.getMentee(mentee.getName(), true) != null){
            result.rejectValue("name", "duplicate", "already exists");
        }
        mentor.addMentee(mentee);
        if (result.hasErrors()) {
            model.put("mentee", mentee);
            return VIEWS_MENTEES_CREATE_OR_UPDATE_FORM;
        } else {
            this.mentees.save(mentee);
            return "redirect:/mentors/{mentorId}";
        }
    }

    @GetMapping("/mentees/{menteeId}/edit")
    public String initUpdateForm(@PathVariable("menteeId") int menteeId, ModelMap model) {
        Mentee mentee = this.mentees.findById(menteeId);
        model.put("mentee", mentee);
        return VIEWS_MENTEES_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/mentees/{menteeId}/edit")
    public String processUpdateForm(@Valid Mentee mentee, BindingResult result, Mentor mentor, ModelMap model) {
        if (result.hasErrors()) {
            mentee.setMentor(mentor);
            model.put("mentee", mentee);
            return VIEWS_MENTEES_CREATE_OR_UPDATE_FORM;
        } else {
            mentor.addMentee(mentee);
            this.mentees.save(mentee);
            return "redirect:/mentors/{mentorId}";
        }
    }

}
