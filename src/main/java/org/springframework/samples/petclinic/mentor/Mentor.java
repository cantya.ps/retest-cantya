/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.mentor;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.core.style.ToStringCreator;
import org.springframework.samples.petclinic.model.Person;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import java.util.*;

/**
 * Simple JavaBean domain object representing an owner.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Michael Isvy
 */
@Entity
@Table(name = "mentors")
public class Mentor extends Person {
    @Column(name = "address")
    @NotEmpty
    private String address;

    @Column(name = "city")
    @NotEmpty
    private String city;

    @Column(name = "telephone")
    @NotEmpty
    @Digits(fraction = 0, integer = 10)
    private String telephone;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mentor")
    private Set<Mentee> mentees;

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    protected Set<Mentee> getMenteesInternal() {
        if (this.mentees == null) {
            this.mentees = new HashSet<>();
        }
        return this.mentees;
    }

    protected void setMenteesInternal(Set<Mentee> mentees) {
        this.mentees = mentees;
    }

    public List<Mentee> getMentees() {
        List<Mentee> sortedMentees = new ArrayList<>(getMenteesInternal());
        PropertyComparator.sort(sortedMentees,
                new MutableSortDefinition("name", true, true));
        return Collections.unmodifiableList(sortedMentees);
    }

    public void addMentee(Mentee mentee) {
        if (mentee.isNew()) {
            getMenteesInternal().add(mentee);
        }
        mentee.setMentor(this);
    }

    /**
     * Return the Pet with the given name, or null if none found for this Owner.
     *
     * @param name to test
     * @return true if pet name is already in use
     */
    public Mentee getMentee(String name) {
        return getMentee(name, false);
    }

    /**
     * Return the Pet with the given name, or null if none found for this Owner.
     *
     * @param name to test
     * @return true if pet name is already in use
     */
    public Mentee getMentee(String name, boolean ignoreNew) {
        name = name.toLowerCase();
        for (Mentee mentee : getMenteesInternal()) {
            if (!ignoreNew || !mentee.isNew()) {
                String compName = mentee.getName();
                compName = compName.toLowerCase();
                if (compName.equals(name)) {
                    return mentee;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)

                .append("id", this.getId()).append("new", this.isNew())
                .append("lastName", this.getLastName())
                .append("firstName", this.getFirstName()).append("address", this.address)
                .append("city", this.city).append("telephone", this.telephone).toString();
    }
}
