/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.mentor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Map;

/**
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 * @author Michael Isvy
 */
@Controller
class MentorController {

    private static final String VIEWS_MENTOR_CREATE_OR_UPDATE_FORM = "mentors/createOrUpdateMentorForm";
    private final MentorRepository mentors;


    public MentorController(MentorRepository clinicService) {
        this.mentors = clinicService;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @GetMapping("/mentors/new")
    public String initCreationForm(Map<String, Object> model) {
        Mentor mentor = new Mentor();
        model.put("mentor", mentor);
        return VIEWS_MENTOR_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/mentors/new")
    public String processCreationForm(@Valid Mentor mentor, BindingResult result) {
        if (result.hasErrors()) {
            return VIEWS_MENTOR_CREATE_OR_UPDATE_FORM;
        } else {
            this.mentors.save(mentor);
            return "redirect:/mentors/" + mentor.getId();
        }
    }

    @GetMapping("/mentors/find")
    public String initFindForm(Map<String, Object> model) {
        model.put("mentor", new Mentor());
        return "mentors/findMentors";
    }

    @GetMapping("/mentors")
    public String processFindForm(Mentor mentor, BindingResult result, Map<String, Object> model) {

        // allow parameterless GET request for /mentors to return all records
        if (mentor.getLastName() == null) {
            mentor.setLastName(""); // empty string signifies broadest possible search
        }

        // find mentors by last name
        Collection<Mentor> results = this.mentors.findByLastName(mentor.getLastName());
        if (results.isEmpty()) {
            // no mentors found
            result.rejectValue("lastName", "notFound", "not found");
            return "mentors/findMentors";
        } else if (results.size() == 1) {
            // 1 owner found
            mentor = results.iterator().next();
            return "redirect:/mentors/" + mentor.getId();
        } else {
            // multiple mentors found
            model.put("selections", results);
            return "mentors/mentorsList";
        }
    }

    @GetMapping("/mentors/{mentorId}/edit")
    public String initUpdateMentorForm(@PathVariable("mentorId") int mentorId, Model model) {
        Mentor mentor = this.mentors.findById(mentorId);
        model.addAttribute(mentor);
        return VIEWS_MENTOR_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/mentors/{mentorId}/edit")
    public String processUpdateMentorForm(@Valid Mentor mentor, BindingResult result, @PathVariable("mentorId") int mentorId) {
        if (result.hasErrors()) {
            return VIEWS_MENTOR_CREATE_OR_UPDATE_FORM;
        } else {
            mentor.setId(mentorId);
            this.mentors.save(mentor);
            return "redirect:/mentors/{mentorId}";
        }
    }

    /**
     * Custom handler for displaying an owner.
     *
     * @param mentorId the ID of the owner to display
     * @return a ModelMap with the model attributes for the view
     */
    @GetMapping("/mentors/{mentorId}")
    public ModelAndView showMentor(@PathVariable("mentorId") int mentorId) {
        ModelAndView mav = new ModelAndView("mentors/mentorDetails");
        mav.addObject(this.mentors.findById(mentorId));
        return mav;
    }

}
